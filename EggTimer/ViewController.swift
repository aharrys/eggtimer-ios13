//
//  ViewController.swift
//  EggTimer
//
//  Created by Angela Yu on 08/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit
import LoremIpsum

class ViewController: UIViewController {
    
    // Initialize Variable of Property
    var totalTime = 0
    var seccondPassed = 0
    var timering = Timer()
    var variationEgg = ["Soft": 3, "Medium":5, "Hard": 10]
    @IBOutlet weak var LabelDone: UILabel!
    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet weak var totalTimer: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    let words: String = LoremIpsum.words(withNumber: 5)
    var TitleScreen : String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "InputVCToDisplayVC" {
            let displayVC = segue.destination as! ViewController2
            displayVC.name = TitleScreen
        }
    }
    
    @IBAction func keyPressed(_ sender: UIButton) {
        seccondPassed = 0
        timering.invalidate()
        progressView.progress = 0.0
        LabelDone.text = sender.currentTitle
        labelTimer.text = String (seccondPassed)
        totalTimer.text = String (totalTime)
        let hardness = sender.currentTitle!
        timering =   Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(processTimer), userInfo: nil, repeats: true)
        totalTime = variationEgg[hardness]!
   //     self.performSegue(withIdentifier: "InputVCToDisplayVC", sender: self)
        print (sender.currentTitle!)
        TitleScreen = sender.currentTitle
        
    }
    // Function for counting process timer
    @objc func processTimer(){
        if seccondPassed < totalTime {
            seccondPassed += 1
            let persentage = Float (seccondPassed) / Float (totalTime)
            progressView.progress = persentage
            labelTimer.text = String (seccondPassed)
            totalTimer.text = String (totalTime)
            print(seccondPassed)
        } else {
            timering.invalidate()
            LabelDone.text = "Done ! ! !"
        }
    }
}
