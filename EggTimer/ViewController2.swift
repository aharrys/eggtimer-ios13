//
//  ViewController2.swift
//  EggTimer
//
//  Created by Akhmad Harry Susanto on 30/06/20.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation
import UIKit
import LoremIpsum

class ViewController2: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    var name : String?
    
    let words: String = LoremIpsum.words(withNumber: 100)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = name
        label.text = words
        label.lineBreakMode = .byCharWrapping
        label.numberOfLines = 20
    }

}
